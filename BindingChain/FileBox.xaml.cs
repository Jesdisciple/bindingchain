﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Microsoft.Win32;

using NLog;

namespace BindingChain
{
	/// <summary>
	/// Interaction logic for FileBox.xaml
	/// </summary>
	public partial class FileBox : UserControl, INotifyPropertyChanged
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		#region TextWidth
		public static DependencyProperty TextWidthProperty =
			DependencyProperty.Register("TextWidth", typeof(GridLength), typeof(FileBox));
		public GridLength TextWidth
		{
			get { return (GridLength)GetValue(TextWidthProperty); }
			set
			{
				SetValue(TextWidthProperty, value);
				NotifyPropertyChanged(() => TextWidth);
			}
		}
		#endregion

		#region ButtonWidth
		public static DependencyProperty ButtonWidthProperty =
			DependencyProperty.Register("ButtonWidth", typeof(GridLength), typeof(FileBox));
		public GridLength ButtonWidth
		{
			get { return (GridLength)GetValue(ButtonWidthProperty); }
			set
			{
				SetValue(ButtonWidthProperty, value);
				NotifyPropertyChanged(() => ButtonWidth);
			}
		}
		#endregion

		#region Filename
		public static readonly DependencyProperty FilenameProperty =
			DependencyProperty.Register("Filename", typeof(string), typeof(FileBox),
			new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, FilenameChanged, FilenameGet));

		private static void FilenameChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			FileBox box = d as FileBox;
			if (box != null && e.NewValue != null)
			{
				var expr = BindingOperations.GetBindingExpression(box, e.Property);
				expr.UpdateSource();
			}
		}

		private static object FilenameGet(DependencyObject d, object baseValue)
		{
			return baseValue;
		}

		public string Filename
		{
			get { return (string)GetValue(FilenameProperty); }
			set
			{
				SetValue(FilenameProperty, value);
				NotifyPropertyChanged(() => Filename);
			}
		}
		#endregion

		private SaveFileDialog FileBrowser;

		public FileBox()
		{
			InitializeComponent();

			FileBrowser = new SaveFileDialog();
		}

		public void Browse(object sender, RoutedEventArgs args)
		{
			FileBrowser.InitialDirectory = System.IO.Path.GetDirectoryName(Filename);
			FileBrowser.FileName = Filename;

			bool? result = FileBrowser.ShowDialog();
			if (result.HasValue && result.Value)
			{
				Filename = FileBrowser.FileName;
			}
		}

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		private void NotifyPropertyChanged<T>(Expression<Func<T>> expr)
		{
			NotifyPropertyChanged(Util.GetPropertyName(expr));
		}

		private void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
